package com.nfc.user.dto;



import com.nfc.user.entity.Address;
import com.nfc.user.entity.Role;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest  {

    @NotBlank(message = "first name is required.")
    private String firstName;
    @NotBlank(message = "last name is required.")
    private String lastName;
    @NotBlank(message = "email is required.")
    @Email
    private String email;
    @NotBlank(message = "password is required.")
    private String password;
    @NotBlank(message = "phone number is required.")
    private String phoneNumber;
    @Valid
    @NotNull(message = "The role is required.")
    private Role role;
    @Valid
    @NotNull(message = "The address is required.")
    private Address personalAddress;


}
