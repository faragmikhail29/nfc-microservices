package com.nfc.user.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResetPasswordDto {


    @NotEmpty(message = "Password is required.")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@#$!%*?&)(])[A-Za-z\\d@#$!%*?&)(]{8,}$",
            message = "Password must be minimum eight characters, at least one uppercase letter, " +
                    "one lowercase letter, one number and one special character")
    private String password;
    @NotEmpty(message = "Confirm Password is required.")
    private String confirmPass;
    private String resetCode;

}
