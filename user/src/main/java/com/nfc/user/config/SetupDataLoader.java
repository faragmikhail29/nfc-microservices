package com.nfc.user.config;



import com.nfc.user.entity.EMail;
import com.nfc.user.entity.Role;
import com.nfc.user.entity.User;
import com.nfc.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class SetupDataLoader implements ApplicationListener<ContextRefreshedEvent> {

   private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private boolean alreadySetup = false;

    @SneakyThrows
    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }
    addUsers("admin","admin", Role.ADMIN);
    addUsers("user","user",Role.COMPANY_ADMIN);


        alreadySetup = true;
    }

    private void addUsers(String email,String password,Role role) {
// add admin
        if(userRepository.findByEmailAddress(email).isEmpty()){
            User admin= User.builder()
                    .email(new EMail(email, UUID.randomUUID(),true))
                    .password(passwordEncoder.encode(password))
                    .role(role)
                    .build();
            userRepository.save(admin);
        }

    }

}