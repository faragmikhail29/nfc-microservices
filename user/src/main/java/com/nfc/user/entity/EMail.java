package com.nfc.user.entity;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EMail {
    private String address;
    private UUID verifyCode;
    @ColumnDefault("false")
    private boolean verified;

}
