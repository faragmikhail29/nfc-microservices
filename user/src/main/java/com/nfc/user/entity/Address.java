package com.nfc.user.entity;

import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class Address  {
    @NotBlank(message = "street is mandatory")
    private String street;
    @NotBlank(message = "house number is required.")
    private String houseNumber;
    @NotBlank(message = "post code is required.")
    private String postCode;
    @NotBlank(message = "city is required.")
    private String city;
    @NotBlank(message = "country is required.")
    private String country;


}
