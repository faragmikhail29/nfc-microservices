package com.nfc.user.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Phone {
    private String number;
    private String verifyCode;
   // @Column(columnDefinition = "boolean not null default false")
   @ColumnDefault("false")
    private boolean verified;

}
