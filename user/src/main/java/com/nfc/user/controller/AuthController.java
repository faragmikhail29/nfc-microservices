package com.nfc.user.controller;


import com.nfc.user.dto.*;
import com.nfc.user.service.AuthService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/users/auth")
public class AuthController {
    @Autowired
    private AuthService service;


    @PostMapping("/register")
    public String addNewUser(@RequestBody @Valid RegisterRequest registerRequest) {
        return service.register(registerRequest);
    }

    @PostMapping("/login")
    public AuthenticationResponse getToken(@RequestBody AuthRequest authRequest) {
        System.out.println("login");
        return service.authenticate(authRequest);
    }

    @CrossOrigin(origins = "http://localhost:8222")
    @GetMapping("/validate")
    public ConnValidationResponse validateToken(@RequestParam("token") String token) {
        return service.validateToken(token);

    }

    @PostMapping("/verify-email/{code}")
    public ResponseEntity<?> verify_email(
            @PathVariable UUID code
    ) {
        return service.verify_email(code);
    }



    @PostMapping("/forget-password")
    public ResponseEntity<?> forget_password(
            @RequestBody ForgetPassRequest request
    ) {
        return service.forget_password(request);
    }



    @PostMapping("/reset-password")
    public ResponseEntity<?> resetPassword(
            @RequestBody @Valid ResetPasswordDto resetPasswordDto
    ) {
        return (service.resetPassword(resetPasswordDto));
    }


}
