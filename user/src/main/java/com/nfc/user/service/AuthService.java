package com.nfc.user.service;


import com.nfc.user.dto.*;
import com.nfc.user.entity.Address;
import com.nfc.user.entity.EMail;
import com.nfc.user.entity.Phone;
import com.nfc.user.entity.User;
import com.nfc.user.repository.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthService  {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final EmailService emailService;

    public String register(RegisterRequest registerRequest) {
        // validate


        var address = Address.builder()
                .city(registerRequest.getPersonalAddress().getCity())
                .country(registerRequest.getPersonalAddress().getCountry())
                .houseNumber(registerRequest.getPersonalAddress().getHouseNumber())
                .street(registerRequest.getPersonalAddress().getStreet())
                .postCode(registerRequest.getPersonalAddress().getPostCode())
                .build();
        User user = User.builder()
                .firstName(registerRequest.getFirstName())
                .lastName(registerRequest.getLastName())
                .email(new EMail(registerRequest.getEmail(), UUID.randomUUID(),false))
                .password(passwordEncoder.encode(registerRequest.getPassword()))
                .role(registerRequest.getRole())
                .phone(new Phone( registerRequest.getPhoneNumber(),"",false))
                .address(address)
                .build();
        repository.save(user);
        emailService.sendVerifyEmail(user.getEmail());
        return "user added to the system";

    }

    public AuthenticationResponse authenticate(AuthRequest request) {
        Authentication authenticate = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );
        var user = repository.findByEmailAddress(request.getUsername())
                .orElseThrow();
        List<String> userRoles = new ArrayList<>();
        userRoles.add(user.getRole().name());
        var jwtToken = jwtService.generateToken(user.getUsername(), userRoles);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .roles(
                        authenticate.getAuthorities().stream()
                                .map(GrantedAuthority::getAuthority)
                                .collect(Collectors.toList()))
                .build();
    }

    public ConnValidationResponse validateToken(String token) {
        Jws<Claims> claimsJws = jwtService.validateToken(token);

        List<String> roles= (List<String>) claimsJws.getBody().get("roles");
        System.out.println(roles);
        List<GrantedAuthority> authorities=new ArrayList<>();
        roles.forEach(r-> authorities.add(new SimpleGrantedAuthority(r)));
        return ConnValidationResponse.builder()
                .isAuthenticated(true)
                .username(claimsJws.getBody().getSubject())
                .authorities(authorities)
                .status("OK")
                .build();

    }

    public ResponseEntity<?> verify_email(UUID code) {
        Optional<User> user = repository.findByEmailVerifyCode(code);
        if (user.isEmpty())
            return new ResponseEntity<>("Invalid verification code.", HttpStatus.CONFLICT);
        user.get().getEmail().setVerified(true);
        repository.save(user.get());
        return new ResponseEntity<>("Your E-Mail is verified.", HttpStatus.OK);
    }

    public ResponseEntity<?> forget_password(ForgetPassRequest request) {
        Optional<User> user = repository.findByEmailAddress(request.getEmail());
        if (user.isEmpty())
            return new ResponseEntity<>("Invalid Email Address.", HttpStatus.CONFLICT);

        user.get().setResetPasswordCode(UUID.randomUUID().toString());
        repository.save(user.get());
        // send rest mail
        emailService.sendRestPassEmail(request.getEmail(), user.get().getResetPasswordCode());

        return new ResponseEntity<>("We send an E-Mail to reset your password.", HttpStatus.OK);

    }

    public ResponseEntity<?> resetPassword(@Valid ResetPasswordDto resetPasswordDto) {
        Optional<User> user = repository.findByResetPasswordCode(resetPasswordDto.getResetCode());
        if (user.isEmpty())
            return new ResponseEntity<>("Invalid Reset Password Code.", HttpStatus.CONFLICT);
        if (!resetPasswordDto.getPassword().equals(resetPasswordDto.getConfirmPass()))
            return new ResponseEntity<>("Password not match!", HttpStatus.CONFLICT);

        user.get().setPassword(passwordEncoder.encode(resetPasswordDto.getPassword()));
        repository.save(user.get());
        return new ResponseEntity<>("Your Password has been reset successfully .", HttpStatus.OK);
    }

}
