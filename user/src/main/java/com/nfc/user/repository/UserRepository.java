package com.nfc.user.repository;


import com.nfc.user.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmailAddress(String username);

    Optional<User> findByEmailVerifyCode(UUID email_verifyCode);

    Optional<User> findByResetPasswordCode(String resetCode);
}
