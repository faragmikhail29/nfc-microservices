package com.nfc.plan.domain.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlanRequest  {
    @NotBlank(message = "password is required.")
    private String name;
    @NotBlank(message = "password is required.")
    private String description;
    @NotNull
    @Min(value = 1, message = " cards number have not to zero or less")
    private Double price;
    @NotNull
    @Min(value = 1, message = "duration have not to zero or less ")
    private Integer duration;
    @NotNull
    @Min(value = 1, message = " cards number have not to zero or less")
    private Integer cardNumber;
    @NotNull
    // @Pattern(regexp = "^true$|^false$", message = "allowed input: true or false")
    private Boolean isEnabled;


}
