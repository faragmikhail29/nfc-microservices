package com.nfc.plan.domain.dto;

import lombok.*;

import java.util.UUID;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlanResponse {
    private UUID id;
    private String name;
    private String description;
    private Double price;
    private Integer duration;
    private Integer cardNumber;
    private Boolean isEnabled;
}
