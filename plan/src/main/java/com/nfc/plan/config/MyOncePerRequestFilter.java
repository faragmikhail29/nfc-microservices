package com.nfc.plan.config;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class MyOncePerRequestFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        String username = request.getHeader("username");
        System.out.println("username: "+ request.getHeader("username"));
        System.out.println("authorities: "+ request.getHeader("authorities"));
        String[] roles = request.getHeader("authorities").split(",");
        List<GrantedAuthority> authorities = new ArrayList<>();
//        roles.forEach(r-> authorities.add(new SimpleGrantedAuthority(r)));
        for (String r : roles)
            if (!r.isEmpty())
                authorities.add(new SimpleGrantedAuthority(r));

        Authentication authentication = new UsernamePasswordAuthenticationToken(username, null, authorities);
        System.out.println(authentication);
        System.out.println("00000000000000000000000000000000000000000");
        SecurityContextHolder.getContext().setAuthentication(authentication);
        filterChain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilterAsyncDispatch() {
        return true;
    }
}