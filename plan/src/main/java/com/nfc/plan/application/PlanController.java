package com.nfc.plan.application;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/plans")
@RequiredArgsConstructor
public class PlanController {
    private final PlanService planService;

    @GetMapping("/test")
    public String getAuth() {
        System.out.println( "From Controller :-");
        System.out.println( SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
        SecurityContextHolder.getContext().getAuthentication().getAuthorities().forEach(r->{
            System.out.println(r.getAuthority());
        });
        return "test from plan";
    }


}

