package com.nfc.gateway.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class Authorities {

    private String authority;
}
