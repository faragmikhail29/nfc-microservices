package com.nfc.gateway.filter;


import com.nfc.gateway.model.Authorities;
import com.nfc.gateway.model.ConnValidationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Component

public class AuthenticationFilter extends AbstractGatewayFilterFactory<AuthenticationFilter.Config> {

    private final WebClient.Builder webClient;
    @Autowired
    private RouteValidator validator;


    public AuthenticationFilter(WebClient.Builder webClient) {
        super(Config.class);
        this.webClient = webClient;
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
//            System.out.println("GatewayFilter");
            if (validator.isSecured.test(exchange.getRequest())) {
                //header contains token or not
                if (!exchange.getRequest().getHeaders().containsKey(HttpHeaders.AUTHORIZATION)) {
                    throw new RuntimeException("missing authorization header");
                }

                String authHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
                if (authHeader != null && authHeader.startsWith("Bearer ")) {
                    authHeader = authHeader.substring(7);
                }
                try {
//                    System.out.println(exchange.getRequest().getPath().contextPath().value());
                    //REST call to AUTH service
                    // String auth=  template.getForObject("http://authentication/auth/validate?token=" + authHeader, String.class);
                    String finalAuthHeader = authHeader;
                return webClient.build().get()
                            .uri("lb://user-service/users/auth/validate",
                                    uriBuilder -> uriBuilder.queryParam("token", finalAuthHeader).build())
                        .retrieve().bodyToMono(ConnValidationResponse.class)
                        .map(response -> {
//                            System.out.println(response);
//                            response.getAuthorities().forEach(r->{
//                                System.out.println(r.getAuthority());
//                            });
                            exchange.getRequest().mutate().header("username", response.getUsername());
                            exchange.getRequest().mutate().header("authorities", response.getAuthorities().stream().map(Authorities::getAuthority).reduce("", (a, b) -> a + "," + b));
                            exchange.getRequest().mutate().header("auth-token", response.getToken());

                            return exchange;
                        }).flatMap(chain::filter).onErrorResume(error -> {
                            return null;
                        });
//                    Jws<Claims> claimsJws = jwtUtil.validateToken(authHeader);
//                    System.out.println(claimsJws);

                } catch (Exception e) {
                    System.out.println("invalid access...!" + e.getMessage());
                    throw new RuntimeException("un authorized access to application");
                }
            }
            return chain.filter(exchange);
        };
    }

    public static class Config {

    }
}
